# Base_datos
### sql 
-- Crear base de datos
CREATE DATABASE EmpresaDB;
GO
USE EmpresaDB;
GO

-- Crear tabla Departamento
CREATE TABLE Departamento (
    IDDepartamento INT IDENTITY(1,1) PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL
);

-- Crear tabla Empleado
CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50) NOT NULL,
    IDJefe INT,
    FechaContratacion DATE NOT NULL,
    Salario DECIMAL(10, 2) NOT NULL,
    Comision DECIMAL(10, 2),
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla Proyecto
CREATE TABLE Proyecto (
    IDProyecto INT IDENTITY(1,1) PRIMARY KEY,
    NombreProyecto VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL,
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla EmpleadoProyecto
CREATE TABLE EmpleadoProyecto (
    IDEmpleado INT NOT NULL,
    IDProyecto INT NOT NULL,
    HorasTrabajadas INT NOT NULL,
    PRIMARY KEY (IDEmpleado, IDProyecto),
    FOREIGN KEY (IDEmpleado) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDProyecto) REFERENCES Proyecto(IDProyecto)
);

-- Insertar registros en la tabla Departamento
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('OPERATIONS', 'BOSTON');

-- Insertar registros en la tabla Empleado sin jefes
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JONES', 'MANAGER', 1, '1981-04-02', 2975, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('BLAKE', 'MANAGER', 1, '1981-05-01', 2850, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('CLARK', 'MANAGER', 1, '1981-06-09', 2450, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SCOTT', 'ANALYST', 2, '1987-04-19', 3000, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('FORD', 'ANALYST', 2, '1981-12-03', 3000, NULL, 2);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SMITH', 'CLERK', 3, '1980-12-17', 800, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ALLEN', 'SALESMAN', 4, '1981-02-20', 1600, 300, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('WARD', 'SALESMAN', 4, '1981-02-22', 1250, 500, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MARTIN', 'SALESMAN', 4, '1981-09-28', 1250, 1400, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('TURNER', 'SALESMAN', 4, '1981-09-08', 1500, 0, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ADAMS', 'CLERK', 5, '1987-05-23', 1100, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JAMES', 'CLERK', 4, '1981-12-03', 950, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MILLER', 'CLERK', 6, '1982-01-23', 1300, NULL, 1);

-- Insertar registros en la tabla Proyecto
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P1', 'BOSTON', 2);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P4', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P5', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P6', 'LOS ANGELES', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P8', 'NEW YORK', 3);

-- Insertar registros en la tabla EmpleadoProyecto
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 2, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 3, 12);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 2, 10);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 5, 8);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 1, 16);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 4, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 5, 5);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (11, 3, 6);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (12, 1, 4);


CREATE LOGIN usuarioEjemplo WITH PASSWORD = 'ContraseñaSegura123!';
CREATE USER usuarioEjemplo FOR LOGIN usuarioEjemplo;

-- Conceder privilegios de selección en la tabla Departamento
GRANT SELECT ON Departamento TO usuarioEjemplo;

-- Conceder privilegios de inserción en la tabla Empleado
GRANT INSERT ON Empleado TO usuarioEjemplo;

-- Conceder privilegios de actualización en la tabla Proyecto
GRANT UPDATE ON Proyecto TO usuarioEjemplo;

-- Revocar privilegios

-- Revocar privilegios de selección en la tabla Departamento
REVOKE SELECT ON Departamento FROM usuarioEjemplo;

-- Revocar privilegios de inserción en la tabla Empleado
REVOKE INSERT ON Empleado FROM usuarioEjemplo;

-- Revocar privilegios de actualización en la tabla Proyecto
REVOKE UPDATE ON Proyecto FROM usuarioEjemplo;


-- COMMIT y ROLLBACK:
BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;

-- SAVEPOINT:

BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVE TRANSACTION Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TRANSACTION Savepoint1;

-- Commit the transaction
COMMIT;


-- SET TRANSACTION:
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

BEGIN TRANSACTION;

INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('New Project', 'CHICAGO', 30);

COMMIT;

CREATE DATABASE EmpleadoDB;
GO

USE EmpleadoDB;
GO

CREATE TABLE Departamento (
    IDDepartamento INT IDENTITY(1,1) PRIMARY KEY,
    NombreDepartamento NVARCHAR(50) NOT NULL UNIQUE,
    Ubicacion NVARCHAR(50) DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INT IDENTITY(1,1) PRIMARY KEY,
    NombreEmpleado NVARCHAR(50) NOT NULL,
    Puesto NVARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT GETDATE(),
    Salario DECIMAL(10,2) CHECK (Salario >= 0),
    Comision DECIMAL(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    CONSTRAINT FK_Empleado_Departamento FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    CONSTRAINT FK_Empleado_Jefe FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);

## MARIADB

-- Crear base de datos
CREATE DATABASE IF NOT EXISTS EmpresaDB;
USE EmpresaDB;

-- Crear tabla Departamento
CREATE TABLE Departamento (
    IDDepartamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL
);

-- Crear tabla Empleado
CREATE TABLE Empleado (
    IDEmpleado INT AUTO_INCREMENT PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50) NOT NULL,
    IDJefe INT,
    FechaContratacion DATE NOT NULL,
    Salario DECIMAL(10, 2) NOT NULL,
    Comision DECIMAL(10, 2),
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla Proyecto
CREATE TABLE Proyecto (
    IDProyecto INT AUTO_INCREMENT PRIMARY KEY,
    NombreProyecto VARCHAR(50) NOT NULL,
    Ubicacion VARCHAR(50) NOT NULL,
    IDDepartamento INT NOT NULL,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla EmpleadoProyecto
CREATE TABLE EmpleadoProyecto (
    IDEmpleado INT NOT NULL,
    IDProyecto INT NOT NULL,
    HorasTrabajadas INT NOT NULL,
    PRIMARY KEY (IDEmpleado, IDProyecto),
    FOREIGN KEY (IDEmpleado) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDProyecto) REFERENCES Proyecto(IDProyecto)
);

-- Insertar registros en la tabla Departamento
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('OPERATIONS', 'BOSTON');

-- Insertar registros en la tabla Empleado sin jefes
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JONES', 'MANAGER', 1, '1981-04-02', 2975, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('BLAKE', 'MANAGER', 1, '1981-05-01', 2850, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('CLARK', 'MANAGER', 1, '1981-06-09', 2450, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SCOTT', 'ANALYST', 2, '1987-04-19', 3000, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('FORD', 'ANALYST', 2, '1981-12-03', 3000, NULL, 2);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SMITH', 'CLERK', 3, '1980-12-17', 800, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ALLEN', 'SALESMAN', 4, '1981-02-20', 1600, 300, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('WARD', 'SALESMAN', 4, '1981-02-22', 1250, 500, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MARTIN', 'SALESMAN', 4, '1981-09-28', 1250, 1400, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('TURNER', 'SALESMAN', 4, '1981-09-08', 1500, 0, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ADAMS', 'CLERK', 5, '1987-05-23', 1100, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JAMES', 'CLERK', 4, '1981-12-03', 950, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MILLER', 'CLERK', 6, '1982-01-23', 1300, NULL, 1);

-- Insertar registros en la tabla Proyecto
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P1', 'BOSTON', 2);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P4', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P5', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P6', 'LOS ANGELES', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P8', 'NEW YORK', 3);

-- Insertar registros en la tabla EmpleadoProyecto
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 2, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 3, 12);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 2, 10);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 5, 8);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 1, 16);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 4, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 5, 5);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (11, 3, 6);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (12, 1, 4);


CREATE USER 'usuarioEjemplo'@'localhost' IDENTIFIED BY 'ContraseñaSegura123!';

-- Conceder privilegios
-- Conceder privilegios de selección en la tabla Departamento
GRANT SELECT ON EmpresaDB.Departamento TO 'usuarioEjemplo'@'localhost';

-- Conceder privilegios de inserción en la tabla Empleado
GRANT INSERT ON EmpresaDB.Empleado TO 'usuarioEjemplo'@'localhost';

-- Conceder privilegios de actualización en la tabla Proyecto
GRANT UPDATE ON EmpresaDB.Proyecto TO 'usuarioEjemplo'@'localhost';

 -- Revocar privilegios

 -- Revocar privilegios de selección en la tabla Departamento
REVOKE SELECT ON EmpresaDB.Departamento FROM 'usuarioEjemplo'@'localhost';

-- Revocar privilegios de inserción en la tabla Empleado
REVOKE INSERT ON EmpresaDB.Empleado FROM 'usuarioEjemplo'@'localhost';

-- Revocar privilegios de actualización en la tabla Proyecto
REVOKE UPDATE ON EmpresaDB.Proyecto FROM 'usuarioEjemplo'@'localhost';


-- COMMIT y ROLLBACK:

START TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;


-- SAVEPOINT:

START TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TO Savepoint1;

-- Commit the transaction
COMMIT;


-- SET TRANSACTION:

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

START TRANSACTION;

INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('New Project', 'CHICAGO', 30);

COMMIT;

CREATE DATABASE EmpleadoDB;
USE EmpleadoDB;

CREATE TABLE Departamento (
    IDDepartamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL UNIQUE,
    Ubicacion VARCHAR(50) DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INT AUTO_INCREMENT PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT CURRENT_DATE,
    Salario DECIMAL(10,2) CHECK (Salario >= 0),
    Comision DECIMAL(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);


## SQLITE

-- Crear tabla Departamento
CREATE TABLE Departamento (
    IDDepartamento INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreDepartamento TEXT NOT NULL,
    Ubicacion TEXT NOT NULL
);

-- Crear tabla Empleado
CREATE TABLE Empleado (
    IDEmpleado INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreEmpleado TEXT NOT NULL,
    Puesto TEXT NOT NULL,
    IDJefe INTEGER,
    FechaContratacion TEXT NOT NULL,
    Salario REAL NOT NULL,
    Comision REAL,
    IDDepartamento INTEGER NOT NULL,
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla Proyecto
CREATE TABLE Proyecto (
    IDProyecto INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreProyecto TEXT NOT NULL,
    Ubicacion TEXT NOT NULL,
    IDDepartamento INTEGER NOT NULL,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento)
);

-- Crear tabla EmpleadoProyecto
CREATE TABLE EmpleadoProyecto (
    IDEmpleado INTEGER NOT NULL,
    IDProyecto INTEGER NOT NULL,
    HorasTrabajadas INTEGER NOT NULL,
    PRIMARY KEY (IDEmpleado, IDProyecto),
    FOREIGN KEY (IDEmpleado) REFERENCES Empleado(IDEmpleado),
    FOREIGN KEY (IDProyecto) REFERENCES Proyecto(IDProyecto)
);

-- Insertar registros en la tabla Departamento
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('ACCOUNTING', 'NEW YORK');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('RESEARCH', 'DALLAS');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('SALES', 'CHICAGO');
INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('OPERATIONS', 'BOSTON');

-- Insertar registros en la tabla Empleado sin jefes
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('KING', 'PRESIDENT', NULL, '1981-11-17', 5000, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JONES', 'MANAGER', 1, '1981-04-02', 2975, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('BLAKE', 'MANAGER', 1, '1981-05-01', 2850, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('CLARK', 'MANAGER', 1, '1981-06-09', 2450, NULL, 1);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SCOTT', 'ANALYST', 2, '1987-04-19', 3000, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('FORD', 'ANALYST', 2, '1981-12-03', 3000, NULL, 2);

-- Insertar registros en la tabla Empleado con jefes ya insertados
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('SMITH', 'CLERK', 3, '1980-12-17', 800, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ALLEN', 'SALESMAN', 4, '1981-02-20', 1600, 300, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('WARD', 'SALESMAN', 4, '1981-02-22', 1250, 500, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MARTIN', 'SALESMAN', 4, '1981-09-28', 1250, 1400, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('TURNER', 'SALESMAN', 4, '1981-09-08', 1500, 0, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('ADAMS', 'CLERK', 5, '1987-05-23', 1100, NULL, 2);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('JAMES', 'CLERK', 4, '1981-12-03', 950, NULL, 3);
INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento) VALUES ('MILLER', 'CLERK', 6, '1982-01-23', 1300, NULL, 1);

-- Insertar registros en la tabla Proyecto
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P1', 'BOSTON', 2);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P4', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P5', 'CHICAGO', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P6', 'LOS ANGELES', 3);
INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('P8', 'NEW YORK', 3);

-- Insertar registros en la tabla EmpleadoProyecto
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 2, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (8, 3, 12);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 2, 10);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (9, 5, 8);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 1, 16);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 4, 15);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (10, 5, 5);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (11, 3, 6);
INSERT INTO EmpleadoProyecto (IDEmpleado, IDProyecto, HorasTrabajadas) VALUES (12, 1, 4);


-- COMMIT y ROLLBACK:

BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;


-- SAVEPOINT:

BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TO Savepoint1;

-- Commit the transaction
COMMIT;


CREATE TABLE Departamento (
    IDDepartamento INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreDepartamento TEXT NOT NULL UNIQUE,
    Ubicacion TEXT DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INTEGER PRIMARY KEY AUTOINCREMENT,
    NombreEmpleado TEXT NOT NULL,
    Puesto TEXT,
    IDJefe INTEGER,
    FechaContratacion DATE DEFAULT (date('now')),
    Salario REAL CHECK (Salario >= 0),
    Comision REAL CHECK (Comision >= 0),
    IDDepartamento INTEGER,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)
);






## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/senati_2024_131/base_datos.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/senati_2024_131/base_datos/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
